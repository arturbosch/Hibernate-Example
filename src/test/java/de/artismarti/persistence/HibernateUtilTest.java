package de.artismarti.persistence;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

/**
 * @author Artur
 */
public class HibernateUtilTest {

	private SessionFactory sessionFactory;

	@Before
	public void setUp() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	@Test
	public void testGetSessionFactory() throws Exception {
		assertThat(sessionFactory, not(nullValue()));
		assertThat(sessionFactory.openSession(), not(nullValue()));
	}

	@Test
	public void testDestroySessionFactory() throws Exception {
		HibernateUtil.destroySessionFactory();
		assertThat(sessionFactory.isClosed(), is(true));
	}
}
