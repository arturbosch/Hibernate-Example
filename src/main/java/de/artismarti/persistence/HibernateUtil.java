package de.artismarti.persistence;

import de.artismarti.model.User;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.jboss.logging.Logger;

/**
 * @author Artur
 */
public class HibernateUtil {

	private static final Logger LOGGER = Logger.getLogger(HibernateUtil.class);

	private static SessionFactory SESSION_FACTORY = createSessionFactory();

	private HibernateUtil() {
	}

	public static SessionFactory getSessionFactory() {
		if (SESSION_FACTORY == null) {
			SESSION_FACTORY = createSessionFactory();
		}
		return SESSION_FACTORY;
	}

	private static SessionFactory createSessionFactory() {
		Configuration configuration = getConfiguration();

		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();

		try {
			return configuration.buildSessionFactory(serviceRegistry);
		} catch (HibernateException ex) {
			LOGGER.error("Could not init session factory!", ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	private static Configuration getConfiguration() {
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		configuration.addAnnotatedClass(User.class);
		return configuration;
	}

	public static void destroySessionFactory() {
		if (SESSION_FACTORY != null) {
			try {
				SESSION_FACTORY.close();
				SESSION_FACTORY = null;
			} catch (HibernateException ex) {
				LOGGER.error("Could not init session factory!", ex);
			}
		}
	}

}
