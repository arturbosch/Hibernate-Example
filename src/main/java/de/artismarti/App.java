package de.artismarti;

import de.artismarti.model.User;
import de.artismarti.persistence.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.util.List;

/**
 * Hello world!
 */
public class App {

	public static void main(String[] args) {
		App app = new App();
		app.run();
	}

	private void run() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		try (Session session = sessionFactory.openSession()) {
			persistPerson(session);
			loadPerson(session);
		}
	}


	private void loadPerson(Session session) {
		try {
			List<User> users = session.createQuery("from User as user where user.username = 'Artur'").list();
			users.forEach(System.out::println);

		} catch (HibernateException ex) {
			rollbackTransactionAndThrowException(session, ex);
		}
	}

	private void persistPerson(Session session) {
		try {

			Transaction transaction = session.getTransaction();
			transaction.begin();

			User user = new User();
			user.setUsername("Artur");
			user.setRegisterDate(LocalDate.now());

			session.save(user);
			transaction.commit();

		} catch (HibernateException ex) {
			rollbackTransactionAndThrowException(session, ex);
		}
	}

	private void rollbackTransactionAndThrowException(Session session, HibernateException ex) {
		session.getTransaction().markRollbackOnly();
		throw ex;
	}
}
