package de.artismarti.model;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Artur
 */
@Entity
@Table(name = "User", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, length = 11)
	private Long ID;

	@Column(name = "Username", nullable = false, length = 20)
	private String username;

	@Column(name = "Register_Date", nullable = false)
	private LocalDate registerDate;

	public Long getID() {
		return ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public LocalDate getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(LocalDate registerDate) {
		this.registerDate = registerDate;
	}

	@Override public String toString() {
		return "User{" +
						"ID=" + ID +
						", username='" + username + '\'' +
						", registerDate=" + registerDate +
						'}';
	}
}
